class girl(object):
    def __init__(self):
        self.__age=18
        self.__weight=70

    @property
    def age(self): 
        if(self.__age>=30):
            return self.__age-10
        elif(self.__age>=40):
            return self.__age-20
        return self.__age

    @age.setter 
    def age(self, age):
        if age >=18 and age<=58:  
            self.__age=age
            print ("age赋值:",self.__age)            
        else:
            print ("请输入一个18到50之间的数！")

    
    @property
    def weight(self):
        return self.__weight

    @weight.setter
    def weight(self, weight):
        self.__weight=weight          
        print ("weight赋值:",self.__weight)


    @weight.deleter 
    def weight(self):
        del self.__weight
        print ("weight is deleted!")


obj = girl()
print (obj.age)  
obj.age=22 
obj.age=31 
if hasattr(obj, 'age')==True:
    print(obj.age) 
else:
    print("obj.age为Null.")   

obj.weight=100

if hasattr(obj, 'weight')==True:
    print("obj.weight is not Null") 
else:
    print("obj.weight 为 Null.")

print(obj.weight)

del obj.weight

if hasattr(obj, 'weight')==True:
    print("obj.weight is not Null") 
    print(obj.weight)
else:
    print("obj.weight 为 Null.")
