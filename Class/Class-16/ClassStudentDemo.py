class Student():
	schoolName="航大"
	studentCount=0
	def __init__(self,name,sex,age=18,lovername='none'):
		self.__name=name
		self.sex=sex
		self.__age=age
		self.__lovername=lovername
		Student.studentCount+=1
		print(self.__name,"我是航大学生,学生人数:",self.studentCount)		

	def setAge(self,age):
		self.__age=age

	def yourName(self):
		print("My Name is ",self.__name)

	def yourSchoolName(self):
		print("My School Name is",self.schoolName)

	def __del__(self):
		Student.studentCount-=1
		print(self.__name,"我不是航大学生,剩余学生:",self.studentCount)		

	def yourAge(self):
		if(self.sex=='f' and self.__age>=30):
			print("I am %d years old"%(self.__age-10))
		else:
			print("I am %d years old"%(self.__age))


obj1=Student("祝佳明","m",17,"冰冰")
obj2=Student("冯意凡","m",18)
obj3=Student("王莎莎","f",30,"祝佳明")
obj4=Student("韩雪婷","f")

print(obj1.sex)
obj1.yourName()
obj3.yourAge()
obj4.yourAge()
obj4.setAge(19)
obj4.yourAge()
print(obj4.schoolName)
obj3.yourSchoolName()
print("学生人数:",Student.studentCount)
del obj3