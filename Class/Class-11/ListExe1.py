#for循环与列表list的结合使用,惯用伎俩:
# print("1+2+3+....+10=?")
#1
iList=[1,2,3,4,5,6,7,8,9,10]
iSum=0
for d in iList:
	iSum+=d
print("方法1:Sum",iSum)
#2

iSum=0
for d in [1,2,3,4,5,6,7,8,9,10]:
	iSum+=d
print("方法2:Sum",iSum)
#3
iList=[1,2,3,4,5,6,7,8,9,10]
iSum=0
for d in range(1,11):
	iSum+=d
print("方法3:Sum",iSum)
#4
iList=[1,2,3,4,5,6,7,8,9,10]
print("方法4:Sum",sum(iList))
#5
iList=[2,1,6,4,51,6,7,18,9,10]
iSum=0
for i in range(0,10):
	iSum+=iList[i]
print("方法5:Sum",iSum)

myList=[]
for i in range(1,6):
	strTip="Please Input No."+str(i)+" Score:"
	myList.append(float(input(strTip)))
print(myList)
print("Sum:",sum(myList))
print("Average:",sum(myList)/len(myList))
