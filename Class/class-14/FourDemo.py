# break
# for i in range(10):
#     print("-----%d-----"%i)
#     for j in range(10):
#         if j > 5:
#             break
#         print(j)

#continue
# for i in range(10):
#     print("-----%d-----" %i)
#     for j in range(10):
#         if j > 5 and j <= 8:
#             print("我是continue特殊")
#             continue
#         print(j)

# exit()
# a=9
# if(a>10):
# 	print("exit")
# 	exit()
# else:
# 	print("Hello.")
# print("End")

# pass 占位符

# def htzd_fun(a,b):
# 	pass

# htzd_fun(1,2)

#三元比较符
# a=13
# if(a>0):
# 	print("正数")
# else:
# 	print("非正数")

# a=13
# print("正数" if a>0 else "非正数


# zip
math=[90,100,60]
music=[80,89,65]
c=zip(math,music)
d=list(c)
print(d[1])
