Python 3.3.2 (v3.3.2:d047928ae3f6, May 16 2013, 00:03:43) [MSC v.1600 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> a=[]
>>> b=list()
>>> a
[]
>>> b
[]
>>> c=[10,20,30,40]
>>> print(c)
[10, 20, 30, 40]
>>> d=[33,"good",True,10.33]
>>> print(d)
[33, 'good', True, 10.33]
>>> d[0]
33
>>> d[3]
10.33
>>> d[4]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: list index out of range
>>> d
[33, 'good', True, 10.33]
>>> d[1]="hello"
>>> d
[33, 'hello', True, 10.33]
>>> e=[1,2,3]
>>> f=d+e
>>> f
[33, 'hello', True, 10.33, 1, 2, 3]
>>> e
[1, 2, 3]
>>> e*3
[1, 2, 3, 1, 2, 3, 1, 2, 3]
>>> 2 in e
True
>>> e
[1, 2, 3]
>>> 4 in e
False
>>> e
[1, 2, 3]
>>> d
[33, 'hello', True, 10.33]
>>> d[::-1]
[10.33, True, 'hello', 33]
>>> d[0:2]
[33, 'hello']
>>> d[2:4]
[True, 10.33]
>>> d[2:]
[True, 10.33]
>>> d[0:2]
[33, 'hello']
>>> d[:2]
[33, 'hello']
>>> d[0:4:2]
[33, True]
>>> d
[33, 'hello', True, 10.33]
>>> d.append(55)
>>> d
[33, 'hello', True, 10.33, 55]
>>> d.insert(2,False)
>>> d
[33, 'hello', False, True, 10.33, 55]
>>> d.pop()
55
>>> d
[33, 'hello', False, True, 10.33]
>>> d.pop(1)
'hello'
>>> d
[33, False, True, 10.33]
>>> d.remove(2)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: list.remove(x): x not in list
>>> d
[33, False, True, 10.33]
>>> d.remove(True)
>>> d
[33, False, 10.33]
>>> d.revove(10.33)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'list' object has no attribute 'revove'
>>> d.remove(10.33)
>>> d
[33, False]
>>> d.clear()
>>> d
[]
>>> 