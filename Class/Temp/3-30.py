class Student:
	schoolName="西安航天大学"
	def __init__(self,name,sex,age=6,lovername="无"):
		self.__name=name
		self.sex=sex
		self.__age=age
		self.__lovername=lovername
		print(self.__name,"已成航大学生.")
	def __del__(self):
		print(self.__name,"不是航大学生")
	def yourName(self):
		print("My Name is",self.__name)
	def yourAge(self):
		if(self.sex=='f'and self.__age>=30):
			print("I am %d years old."%((self.__age)-10))
		else:
			print("I am %d years old."%(self.__age))
	def yourSchoolName(self):
		print("My School Name is",self.schoolName)
		
ob1=Student("刘强","m",19)
ob2=Student("王莎莎","f",30)
ob3=Student("刘大伟","m",34,"冰冰")
ob1.yourName()
ob1.yourAge()
ob2.yourAge()
ob3.yourName()
ob3.yourSchoolName()
del ob2
Student.schoolName="中国航天大学"
ob3.yourSchoolName()
