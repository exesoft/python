class Rectangle():
	def __init__(self,width,height):
		self._err=False
		if(width<0):			
			self._err=True
		if(height<0):			
			self._err=True
		if(self._err==False):			
			self._width=width
			self._height=height
		else:
			print("长方形初始化参数有误,修订后此函数方可调用.")

	def printArea(self):
		if(self._err==True):
			return
		print("Area ",self._width*self._height)


class Square(Rectangle):
	def __init__(self,a):
		Rectangle.__init__(self,a,a)
		
		

obj=Square(-1)
obj.printArea()
