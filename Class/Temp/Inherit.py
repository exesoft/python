class Rectangle():
	def __init__(self,width,height):
		self.__err=False
		if(width<0):			
			self.__err=True
		if(height<0):			
			self.__err=True
		if(self.__err==False):			
			self.__width=width
			self.__height=height
		else:
			print("长方形初始化参数有误,修订后此函数方可调用.")

	def printArea(self):
		if(self.__err==True):
			return
		print("Rectangle Area ",self.__width*self.__height)



obj1=Rectangle(3,4)
obj1.printArea()