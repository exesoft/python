class Goods():
    def __init__(self):
        self.value=50
    @property
    def price(self):  # 查看属性
        return self.value

    @price.setter  # 添加或设置属性（属性名.setter）
    def price(self, value):
        if value >=50 and value<=100:  #对属性的取值和赋值加以控制
            self.value=value
            print (self.value)
        else:
            print ("请输入一个50到100之间的数！")

    @price.deleter  # 删除属性（属性名.deleter） 注意：属性一旦删除，就无法设置和获取
    def price(self):
        del self.value
        print ("price is deleted!")

obj = Goods()
print (obj.price)   # 自动执行 @property 修饰的 price 方法，并获取方法的返回值
obj.price=57     # 自动执行 @price.setter 修饰的 price 方法，并将106 赋值给方法
del obj.price     # 自动执行 @price.deleter 修饰的 price 方法