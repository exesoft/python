strTip="Please Input your score:"
print(strTip)
print("-"*len(strTip))
score=float(input())
if score<60:
	print("No Pass")
elif score>=60 and score<70:
	print("Pass")
elif score>=70 and score<80:
	print("Good")
elif score>=80 and score<90:
	print("Better")
else:
    print("Best")
