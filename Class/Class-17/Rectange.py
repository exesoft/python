class Rectangle():
	def __init__(self,a,b):
		if(a>=0 and b>=0):
			self.err=False
		else:
			self.err=True
		self.a=a
		self.b=b

	def getA(self):
		return self.a

	def printArea(self):
		if(self.err==False):
			print("Area:",self.a*self.b)
		else:
			print("初始化参数有误,修订后方可调用printArea函数.")


class Square(Rectangle):
	def __init__(self,a):
		Rectangle.__init__(self,a,a)

	def printClass(self):
		print("I Am Square")

obj1=Square(2)
obj1.printArea()
obj1.printClass()
obj2=Rectangle(3,5)
obj2.printArea()



		