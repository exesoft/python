Python 3.3.2 (v3.3.2:d047928ae3f6, May 16 2013, 00:03:43) [MSC v.1600 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> 1+1
2
>>> 56-45
11
>>> 56.5*2
113.0
>>> 7/2
3.5
>>> 7//2
3
>>> 7%2
1
>>> ((4+2)*3-11)+56/34
8.647058823529411
>>> int(2.33)
2
>>> float(2)
2.0
>>> 'htzd.org'
'htzd.org'
>>> 'htzd'*2
'htzdhtzd'
>>> 'htzd'+'1234'
'htzd1234'
>>> '12'
'12'
>>> str(12)
'12'
>>> int('12')
12
>>> 3>2
True
>>> 3<2
False
>>> (45-67)>0
False
>>> (3>2)
True
>>> (3>2) and False
False
>>> (3>2) and (2>1)
True
>>> (3>2) and (2<1)
False
>>> True or False
True
>>> (3>2) or (2<1)
True
>>> bool(0)
False
>>> bool(-1)
True
>>> bool(12)
True
>>> 