Python 3.3.2 (v3.3.2:d047928ae3f6, May 16 2013, 00:03:43) [MSC v.1600 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> 12
12
>>> 34.45
34.45
>>> 'asb'
'asb'
>>> True
True
>>> False
False
>>> 34-56*7/4
-64.0
>>> 56/5
11.2
>>> 56//5
11
>>> 56%5
1
>>> 4>3
True
>>> 4<3
False
>>> (4>3) and (4>1)
True
>>> (4>3) or (4<1)
True
>>> not True
False
>>> not False
True
>>> not (3>2)
False
>>> a=1
>>> a
1
>>> type(1)
<class 'int'>
>>> type(a)
<class 'int'>
>>> a=12.55
>>> type(a)
<class 'float'>
>>> a
12.55
>>> a='aaa'
>>> a
'aaa'
>>> type(a)
<class 'str'>
>>> 1=1
  File "<stdin>", line 1
SyntaxError: can't assign to literal
>>> 1=2
  File "<stdin>", line 1
SyntaxError: can't assign to literal
>>> b=2
>>> b
2
>>> 125656565565656*232322323232
29192825241567334150120192
>>> 111111111111111111111111*2333333333333333333333333333333
259259259259259259259258999999962962962962962962962963
>>> 23.5555555555555555555555555555555555555
23.555555555555557
>>> 9.8*(10**4)
98000.0
>>> 9.8e4
98000.0
>>> 9.8e100000
inf
>>> 0.1+0.1
0.2
>>> 0.1+0.2
0.30000000000000004
>>> (0.1+0>2)>0.3
False
>>> (0.1+0.2)==0.3
False
>>> 3==3
True
>>> 0.3==0.3
True
>>> (0.1+0.2)==0.3
False
>>> round(1.235,2)
1.24
>>> round(1.2345,3)
1.234
>>> 