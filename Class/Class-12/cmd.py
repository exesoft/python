Python 3.3.2 (v3.3.2:d047928ae3f6, May 16 2013, 00:03:43) [MSC v.1600 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> stack=[]
>>> stack.append(22)
>>> stack
[22]
>>> stack.append(44)
>>> stack.append(66)
>>> stack.append(88)
>>> stack
[22, 44, 66, 88]
>>> stack.pop()
88
>>> stack
[22, 44, 66]
>>> stack.pop()
66
>>> stack
[22, 44]
>>> stack.clear()
>>> stack
[]
>>> 