
### 课程名称:Python编程基础

### 专业:工业机器人

### 教师公众号 格码顶端编程 gCodeTop

![输入图片说明](https://images.gitee.com/uploads/images/2020/0216/161005_0dd66983_5692637.jpeg "在这里输入图片标题")

### Python3代码在线集成环境测试地址

[测试地址A](https://www.dooccn.com/python3/) [测试地址B](http://c.runoob.com/compile/9)

### Python代码在线命令行及集成环境测试地址

[测试地址](http://tools.jb51.net/code/python_Skulpt)

### 在线演示代码备课记录

[这里这里](https://gitee.com/exesoft/dashboard/codes)

### 教案Wiki

[这里这里](https://gitee.com/exesoft/python/wikis/1.%E7%BB%AA%E8%AE%BA?sort_id=1910076)